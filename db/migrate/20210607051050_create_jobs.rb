# frozen_string_literal: true

class CreateJobs < ActiveRecord::Migration[6.1]
  def change
    create_table :jobs do |t|
      t.string :location
      t.datetime :date
      t.integer :parent_id
      t.integer :sitter_id

      t.timestamps
    end
  end
end
