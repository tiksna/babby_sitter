# frozen_string_literal: true

class Job < ApplicationRecord
  belongs_to :parent
  belongs_to :sitter
end
