# frozen_string_literal: true

# This is true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
