# frozen_string_literal: true

class Sitter < ApplicationRecord
  validates :name, presence: true
  validates :years_experience, presence: true
  validates :location, presence: true
  has_many :jobs
  has_many :parents, through: :jobs
end
