# frozen_string_literal: true

class Parent < ApplicationRecord
  validates :name, presence: true
  validates :num_children, presence: true
  validates :location, presence: true
  has_many :jobs
  has_many :sitters, through: :jobs
end
