# frozen_string_literal: true

# This is right

module API
  module V1
    # This is the result class
    class Results < Grape::API
      format :json
      prefix :api

      resources :parents do
        desc 'Return all the parents'
        get '', root: :parents do
          Parent.all
        end

        desc 'Return all sitters'
        get :sitters do
          Sitter.all
        end

        desc 'Return sitter of the parent with id 1'
        get :working do
          Parent.first.sitters
        end

        desc 'Create the parent '
        params do
          requires :name, type: String, desc: 'Name of the parent'
          requires :nchild, type: Integer, desc: 'Number of child'
          requires :location, type: String, desc: 'location of parent'
        end
        post do
          Parent.create!({
                           name: params[:name],
                           num_children: params[:nchild],
                           location: params[:location]
                         })
        end

        desc 'create the sitter'
        params do
          requires :name, type: String, desc: 'Name of the parent'
          requires :yexper, type: Integer, desc: 'Number of child'
          requires :location, type: String, desc: 'location of parent'
        end
        post ':sittercreate' do
          Sitter.create!({
                           name: params[:name],
                           years_experience: params[:nchild],
                           location: params[:location]
                         })
        end

        desc 'Update the parent children'
        params do
          requires :id, type: Integer, desc: 'ID of the parent'
          requires :nchild, type: Integer, desc: 'number of child to change'
        end
        put ':pid' do
          Parent.find(params[:id]).update({
                                            num_children: params[:uchild]
                                          })
        end

        desc 'delete the parent record'
        params do
          requires :id, type: Integer, desc: 'id of the parent'
        end
        delete ':pid' do
          Parent.find(params[:id]).destroy
        end
      end
    end
  end
end
