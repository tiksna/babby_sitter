require 'rails_helper'

RSpec.describe "Results", type: :request do
  it 'return all parent' do 
    get '/api/v1/parents'
    expect(response).to have_http_status(:success)
  end
  
  it 'return all sitter' do 
    get '/api/v1/parents/sitters'
    expect(response).to have_http_status(:success)
  end
  
  it 'working sitter' do 
    get 'api/v1/parents/working'
    expect(response).to have_http_status(:success)
  end
  
  it 'create the parent' do
    post 'api/v1/parents'
    expect(respose).to have_http_status(:success)
  end

  it 'create sitter' do 
    post 'api/v1/parents/:sittercreate'  
      expect(respose).to have_http_status(:success)
    
  end
    
  it 'update parent' do
    update 'api/v1/parents/:1' 
      expect(response).to have_http_status(:success)
  end
  
  it 'delete the parent' do 
    delete 'api/v1/parents/:1'
    expect(response).to have_http_status(:success)
  end  
end
