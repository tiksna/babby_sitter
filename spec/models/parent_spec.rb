# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Parent, type: :model do
  context 'validation tests' do
    it 'check the name' do
      # p = Parent.create(num_children: 4,location: "India").save
      p = build(:parent, name: 'Sakshi', num_children: 2)
      expect(p.location).to eq('India')
    end

    it 'check the num_children' do
      p = Parent.create(name: 'Seema', location: 'India').save
      expect(p).to eq(false)
    end

    it 'check the location' do
      p = Parent.create(name: 'Seema', num_children: 4).save
      expect(p).to eq(false)
    end

    it 'successful as the correct input' do
      p = Parent.create(name: 'Seema Singh', num_children: 4, location: 'India').save
      expect(p).to eq(true)
    end
  end
  context 'scope test' do
  end
end
