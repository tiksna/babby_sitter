# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Sitter, type: :model do
  context 'validation tests' do
    it 'check the name' do
      p = Sitter.create(years_experience: 4, location: 'India').save
      expect(p).to eq(false)
    end

    it 'check the years_experience' do
      p = Sitter.create(name: 'Seema', location: 'India').save
      expect(p).to eq(false)
    end

    it 'check the location' do
      p = Sitter.create(name: 'Seema', years_experience: 4).save
      expect(p).to eq(false)
    end

    it 'successful as the correct input' do
      p = Sitter.create(name: 'Seema Singh', years_experience: 4, location: 'India').save
      expect(p).to eq(true)
    end
  end
  context 'scope test' do
  end
end
