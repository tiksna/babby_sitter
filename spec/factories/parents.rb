# frozen_string_literal: true

# default value of the parameter if not given
FactoryBot.define do
  factory :parent do
    name { 'Suneeta' }
    num_children { 12 }
    location { 'India' }
  end
end
